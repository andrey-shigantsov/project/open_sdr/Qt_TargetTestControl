/*
 * opensdr_assert.cpp
 */

#include <SDR/BASE/common.h>
#ifdef SDR_EXTERN_ASSERT
#include <QtCore>
extern "C" void sdr_assert(const char * expr, const char * file, const int line)
{
  qt_assert(expr, file, line);
}
#endif
