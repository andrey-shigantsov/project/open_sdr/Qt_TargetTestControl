#include "TargetTestApp.h"

#include <SDR/Qt_Addons/Controls/TControlWidgetGenerator.h>
#include <SDR/Qt_Addons/Controls/TStatusWidgetGenerator.h>
#include <SDR/Qt_Addons/Controls/TPlotsWidgetGenerator.h>

#include <SDR/target_interface/internal/io.h>

#include <QCryptographicHash>

using namespace SDR;

extern "C"
{
  Size_t TARGET_IO_raw_send_buf(TARGET_IO_t * IO, UInt8_t * Buf, Size_t Count)
  {
    TargetTestApp * This = (TargetTestApp *)IO->cfg.Master;
    IODevice * dev = This->ioDevice();
    return dev->send((char *)Buf, Count);
  }

  static void target_system_started(TargetTestApp * This)
  {
    qInfo() << "target system started";
    This->getAll();
  }

  static void target_params_changed_handler(TargetTestApp * This, TARGET_Params_t * Params)
  {
    qInfo() << "target params changed {timeout:" << Params->timeout_ms << "}";
    This->ControlWidget()->ui_set_exec_timeout(Params->timeout_ms);
  }

  static void target_state_changed_handler(TargetTestApp * This, TARGET_State_t state)
  {
    if (!This->ControlWidget()->ui_ExecControlsEnabled())
      This->ControlWidget()->ui_set_exec_controls_enabled(true);
    switch(state)
    {
    default:
      break;

    case TARGET_Started:
      qInfo() << "target started";
      This->ControlWidget()->ui_set_exec_state(AbstractTestApp::Started);
      if (This->TargetControlWidget())
        This->TargetControlWidget()->setIsStarted(true);
      This->ControlWidget()->Plots()->clearData();
      break;

    case TARGET_Running:
      qInfo() << "target running";
      This->ControlWidget()->ui_set_exec_state(AbstractTestApp::Running);
      break;

    case TARGET_Paused:
      qInfo() << "target paused";
      This->ControlWidget()->ui_set_exec_state(AbstractTestApp::Started);
      break;

    case TARGET_Stopped:
      qInfo() << "target stopped";
      This->ControlWidget()->ui_set_exec_state(AbstractTestApp::Stopped);
      if (This->TargetControlWidget())
        This->TargetControlWidget()->setIsStarted(false);
      break;
    }
  }

  static void target_name_changed_handler(TargetTestApp * This, const char * name)
  {
    qInfo() << "target name: \"" << name << "\"";
    This->setName(name);
  }

  static void target_control_format_changed_handler(TargetTestApp * This, const char * format)
  {
    qInfo() << "target control format: \"" << format << "\"";
    QByteArray hash = QCryptographicHash::hash(QByteArray::fromRawData(format,strlen(format)), QCryptographicHash::Md5);
    if (!(This->TargetControlWidget() && (hash == This->controlFormatHash())))
      This->setTargetControlWidget(TControlWidgetGenerator::create(format,This->ControlWidget()), hash);
  }

  static void target_status_format_changed_handler(TargetTestApp * This, const char * format)
  {
    qInfo() << "target status format: \"" << format << "\"";
    QByteArray hash = QCryptographicHash::hash(QByteArray::fromRawData(format,strlen(format)), QCryptographicHash::Md5);
    if (!(This->TargetStatusWidget() && (hash == This->statusFormatHash())))
       This->setTargetStatusWidget(TStatusWidgetGenerator::create(format,This->ControlWidget()), hash);
  }

  static void target_plots_format_changed_handler(TargetTestApp * This, const char * format)
  {
    qInfo() << "target plots format: \"" << format << "\"";
    QByteArray hash = QCryptographicHash::hash(QByteArray::fromRawData(format,strlen(format)), QCryptographicHash::Md5);
    if (hash == This->plotsFormatHash())
      return;
    This->setPlotsFormatHash(hash);
    This->PlotsById()->clear();
    This->ControlWidget()->Plots()->clear();
    if (TPlotsWidgetGenerator::read(format, This->ControlWidget()->Plots(), This->PlotsById()))
      This->ControlWidget()->ui_set_plots_enabled(true);
  }

  static void target_element_value_changed_handler(TargetTestApp * This, TARGET_ElementType_t type, TARGET_DataId_t id, const char * value)
  {
    switch (type)
    {
    case TARGET_ControlElement:
      qInfo() << "target control element #" << id << " value: \"" << value << "\"";
      if (!This->TargetControlWidget()) break;
      This->TargetControlWidget()->setValue(id, value);
      break;
    case TARGET_StatusElement:
      qInfo() << "target status element #" << id << " value: \"" << value << "\"";
      if (!This->TargetStatusWidget()) break;
      This->TargetStatusWidget()->setValue(id, value);
      break;
    }
  }

  static void target_element_params_changed_handler(TargetTestApp * This, TARGET_ElementType_t type, TARGET_DataId_t id, const char * params)
  {
    switch (type)
    {
    case TARGET_ControlElement:
      qInfo() << "target control element #" << id << " params: \"" << params << "\"";
      if (!This->TargetControlWidget()) break;
      This->TargetControlWidget()->setParams(id, params);
      break;
    }
  }

  static void target_plot_command_ready_handler(TargetTestApp * This, TARGET_DataId_t id, TARGET_PlotCommand_t cmd, const char * ext)
  {
    if (!This->PlotsById()->contains(id))
    {
      qWarning() << "target plot command handler: unknown id #" << id;
      return;
    }
    TPlot * p = This->PlotsById()->value(id);
    switch(cmd)
    {
    case TARGET_plotCmd_Clear:
      qInfo() << "target clear plot #" << id;
      p->clear();
      break;
    case TARGET_plotCmd_setParam:
      qInfo() << "target set plot #" << id << "param: {" << ext << "}";
      p->readParams(ext);
      break;
    default:
      qWarning() << "target plot command handler: unknown cmd #" << cmd;
      break;
    }
  }

  static void target_rx_samples_handler(TargetTestApp * This, TARGET_DataId_t id, Sample_t * buf, Size_t count)
  {
    if (This->PlotsById()->contains(id))
    {
      QList<TPlot*> values = This->PlotsById()->values(id);
      foreach(TPlot* plot, values)
        plot->append(buf,count);
    }
    else
      qWarning() << "TargetTestApp: rx samples handler: unknown id #" << id;
  }

  static void target_rx_iq_samples_handler(TargetTestApp * This, TARGET_DataId_t id, iqSample_t * buf, Size_t count)
  {
    if (This->PlotsById()->contains(id))
    {
      QList<TPlot*> values = This->PlotsById()->values(id);
      foreach(TPlot* plot, values)
        plot->append(buf,count);
    }
    else
      qWarning() << "TargetTestApp: rx iq samples handler: unknown id #" << id;
  }

  static void target_rx_user_handler(TargetTestApp * This, TARGET_DataId_t id, UInt8_t * data, Size_t count)
  {

  }

  static void target_log_message_handler(TargetTestApp * This, const char * message)
  {
    This->ControlWidget()->Logs()->append(message);
  }
}

#define DEFAULT_NAME "SDR TARGET Test Control"

TargetTestApp::TargetTestApp(int timeout_ms, QString interfaceStr) :
  AbstractTestApp(timeout_ms, DEFAULT_NAME)
{
  ioDevicePtr = 0;

  TARGET_IO_Cfg_t cfgIO;
  cfgIO.Master = this;
  cfgIO.on_system_started = (TARGET_system_started_fxn_t)target_system_started;
  cfgIO.on_params_changed = (TARGET_params_changed_fxn_t)target_params_changed_handler;
  cfgIO.on_state_changed = (TARGET_state_changed_fxn_t)target_state_changed_handler;
  cfgIO.on_name_changed = (TARGET_name_changed_fxn_t)target_name_changed_handler;
  cfgIO.on_status_format_changed = (TARGET_format_changed_fxn_t)target_status_format_changed_handler;
  cfgIO.on_control_format_changed = (TARGET_format_changed_fxn_t)target_control_format_changed_handler;
  cfgIO.on_plots_format_changed = (TARGET_format_changed_fxn_t)target_plots_format_changed_handler;
  cfgIO.on_element_value_changed = (TARGET_element_value_changed_fxn_t)target_element_value_changed_handler;
  cfgIO.on_element_params_changed = (TARGET_element_params_changed_fxn_t)target_element_params_changed_handler;
  cfgIO.on_plot_command_ready = (TARGET_plot_command_ready_fxn_t)target_plot_command_ready_handler;
  cfgIO.on_samples_ready = (TARGET_samples_ready_fxn_t)target_rx_samples_handler;
  cfgIO.on_iq_samples_ready = (TARGET_iq_samples_ready_fxn_t)target_rx_iq_samples_handler;
  cfgIO.on_log_message = (TARGET_log_message_fxn_t)target_log_message_handler;
  init_TARGET_IO(&io, &cfgIO);
  TARGET_IO_Callback_t cbkIO;
  cbkIO.rx_UserData_ready = (TARGET_IO_rx_UserData_ready_t)target_rx_user_handler;
  TARGET_IO_setCallback(&io, this, &cbkIO);

  TargetCW = nullptr;
  TargetSW = nullptr;

  ControlWidget()->appendUserWidget(&ioCW);

  connect(&ioCW, SIGNAL(connectionControlClicked()), this, SLOT(ConnectionControlHandler()));
  connect(&ioCW, SIGNAL(interfaceChanged(InterfaceControlWidget::Interface)), this, SLOT(refreshInterface(InterfaceControlWidget::Interface)));

  ioCW.setInterface(interfaceStr);
  ioCW.setComBaudRate(115200);

  ui_setConnectionState(Disconnected);
}

TargetTestApp::~TargetTestApp()
{
  disconnectInterface();
  if (ioDevicePtr && ioDevicePtr->isOpen())
    ioDevicePtr->close();
}

void TargetTestApp::ConnectionControlHandler()
{
  if (ioDevicePtr->isOpen())
  {
    ioDevicePtr->close();
  }
  else
  {
    refreshInterfaceParams();
    ioCW.setWaitingState(true);
    if (!ioDevicePtr->open())
    {
      ioCW.setWaitingState(false);
      qDebug() << metaObject()->className() << ": connect failure: " << ioDevicePtr->errorString();
    }
  }
}

void TargetTestApp::OpennedHandler()
{
  if (ioDevicePtr == &comPort)
  {
    comPort.setBaudRate(ioCW.comBaudRate());
  }

  ui_setConnectionState(Connected);

  getAll();
}

void TargetTestApp::rxBufHandler(const QByteArray buf)
{
  TARGET_IO_Result_t res = TARGET_IO_raw_rx_buf_handler(&io, (UInt8_t *)buf.data(), (Size_t)buf.count());
  switch (res)
  {
  default: break;
  case target_io_FAIL:
    qDebug() << metaObject()->className() << ": rx buf handler: failure: [" << buf.toHex(' ') << "]";
    break;

  case target_io_FAIL_CRC:
    qDebug() << metaObject()->className() << ": rx buf handler: CRC failure: [" << buf.toHex(' ') << "]";
    break;
  }
}

void TargetTestApp::ClossedHandler()
{
  ui_setConnectionState(Disconnected);
  setName(DEFAULT_NAME);
}

void TargetTestApp::ui_setConnectionState(ConnectionState state)
{
  switch (state)
  {
  case Disconnected:
    ioCW.setConnectionState(false);
    setTargetControlWidget(nullptr);
    setTargetStatusWidget(nullptr);
    PlotsMap.clear();
    pfHash.clear();
    ControlWidget()->Plots()->clear();
    ControlWidget()->Logs()->clear();
    ControlWidget()->Logs()->close();
    ControlWidget()->ui_set_exec_state(AbstractTestApp::Stopped);
    ControlWidget()->ui_set_plots_enabled(false);
    ControlWidget()->ui_set_exec_controls_enabled(false);
    break;
  case Connected:
    ioCW.setConnectionState(true);
    break;
  }
}

void TargetTestApp::setTargetControlWidget(TControlWidget *cw, const QByteArray formatHash)
{
  if (TargetCW)
  {
    disconnect(TargetCW, SIGNAL(valueChanged(int,QString)), this, SLOT(sendValue(int,QString)));
    ControlWidget()->deleteUserWidget(TargetCW);
  }
  TargetCW = cw;
  setControlFormatHash(formatHash);
  if (cw)
  {
    ControlWidget()->appendUserWidget(cw);
    connect(TargetCW, SIGNAL(valueChanged(int,QString)), this, SLOT(sendValue(int,QString)));
  }
}

void TargetTestApp::setTargetStatusWidget(TStatusWidget *sw, const QByteArray formatHash)
{
  if (TargetSW)
  {
    ControlWidget()->deleteUserStatusWidget(TargetSW);
  }
  TargetSW = sw;
  setStatusFormatHash(formatHash);
  if (sw)
  {
    ControlWidget()->appendUserStatusWidget(sw);
  }
}

void SDR::TargetTestApp::init()
{
  sendUserControlValues();
}

void SDR::TargetTestApp::start()
{
  qInfo() << "host start";
  TARGET_IO_send_command(&io, HOST_cmdSTART);
}

void SDR::TargetTestApp::pause()
{
  qInfo() << "host pause";
  TARGET_IO_send_command(&io, HOST_cmdPAUSE);
}

void SDR::TargetTestApp::stop()
{
  qInfo() << "host stop";
  TARGET_IO_send_command(&io, HOST_cmdSTOP);
}

void SDR::TargetTestApp::step()
{
  qInfo() << "host step";
  TARGET_IO_send_command(&io, HOST_cmdSTEP);
}

void TargetTestApp::reset()
{
  qInfo() << "host reset";
  TARGET_IO_send_command(&io, HOST_cmdRESET);

  ControlWidget()->Plots()->clearData();
}

void TargetTestApp::refreshInterfaceParams()
{
  if (ioDevicePtr == &comPort)
  {
    comPort.setPortName(ioCW.comPort());
  }
  else if (ioDevicePtr == &tcpSock)
  {
    tcpSock.setHost(ioCW.tcpHost(), ioCW.tcpPort());
  }
}

void TargetTestApp::getAll()
{
  qInfo() << "host get all";
  TARGET_IO_send_command(&io, HOST_getAll);
}

void TargetTestApp::getFormat()
{
  qInfo() << "host get format";
  TARGET_IO_send_command(&io, HOST_getFormat);
}

void TargetTestApp::getState()
{
  qInfo() << "host get state";
  if (!TARGET_IO_send_command(&io, HOST_getState))
    qDebug() << "host get state failure";
}

void TargetTestApp::sendParams()
{
  qInfo() << "host set params: {timeout " << TimeoutMs << "}";
  TARGET_Params_t params;
  params.timeout_ms = TimeoutMs;
  if (!TARGET_IO_send_params(&io, &params))
    qDebug() << "host send params failure";
}

void TargetTestApp::sendUserControlValues()
{
  if (TargetControlWidget())
  {
    qInfo() << "host set all values";
    TargetControlWidget()->sendAllValues();
  }
}

void TargetTestApp::sendValue(const int id, const QString &val)
{
  qInfo() << "host set value #" << id << "to" << val;
  if (!TARGET_IO_send_value(&io, id, val.toUtf8().data()))
    qDebug() << "host send value failure";
}

void TargetTestApp::disconnectInterface()
{
  if (ioDevicePtr)
  {
    disconnect(ioDevicePtr, SIGNAL(openned()), this, SLOT(OpennedHandler()));
    disconnect(ioDevicePtr, SIGNAL(rx_buf_ready(QByteArray)), this, SLOT(rxBufHandler(QByteArray)));
    disconnect(ioDevicePtr, SIGNAL(clossed()), this, SLOT(ClossedHandler()));
  }
}

void TargetTestApp::refreshInterface(InterfaceControlWidget::Interface val)
{
  disconnectInterface();

  switch (val)
  {
  case InterfaceControlWidget::COM:
    ioDevicePtr = &comPort;
    break;
  case InterfaceControlWidget::TCP:
    ioDevicePtr = &tcpSock;
    break;
  }

  connect(ioDevicePtr, SIGNAL(openned()), this, SLOT(OpennedHandler()));
  connect(ioDevicePtr, SIGNAL(rx_buf_ready(QByteArray)), this, SLOT(rxBufHandler(QByteArray)));
  connect(ioDevicePtr, SIGNAL(clossed()), this, SLOT(ClossedHandler()));
}


