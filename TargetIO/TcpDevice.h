#ifndef TCPDEVICE_H
#define TCPDEVICE_H

#include "IODevice.h"
#include "ProtoReader.h"

#include <QTcpSocket>
#include <QHostAddress>

class TcpDevice : public IODevice
{
  Q_OBJECT
public:
  explicit TcpDevice(QObject * parent = nullptr);

  bool isOpen();

  QString errorString();

  void setHost(QString hostname, quint16 port);

public slots:
  bool open();
  void close();
  qint64 send(const char * data, qint64 size);

protected:
  QTcpSocket tcpSocket;
#ifdef SDR_TARGET_TEST_APP_TCP_FIND_HOSTADDR
  QHostAddress hostaddr;
#else
  QString hostname;
#endif
  quint16 port;

  ProtoReader protoReader;

protected slots:
  void tcpsocket_statehandler(QAbstractSocket::SocketState);
  void tcpsocket_read();
  void proto_buf_handler(QByteArray buf);
};

#endif // TCPDEVICE_H
