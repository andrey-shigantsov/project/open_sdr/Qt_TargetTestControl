#include "SerialPortDevice.h"

//#define RX_TRACE

#include <QDebug>

#include <SDR/target_interface/base/proto.h>

SerialPortDevice::SerialPortDevice(QObject *parent) :
  IODevice(parent)
{
  connect(&protoReader, SIGNAL(buf_ready(QByteArray)), this, SLOT(protoBufHandler(QByteArray)));
}

bool SerialPortDevice::open()
{
  bool res = port.open(QIODevice::ReadWrite);
  if (res)
    OpennedHandler();
  return res;
}

void SerialPortDevice::close()
{
  ClossedHandler();
  port.close();
}

void SerialPortDevice::setPortName(QString name)
{
  port.setPortName(name);
}

void SerialPortDevice::setBaudRate(qint32 rate)
{
  port.setBaudRate(rate);
}

void SerialPortDevice::OpennedHandler()
{
  port.setDataBits(QSerialPort::Data8);
  port.setParity(QSerialPort::NoParity);
  port.setStopBits(QSerialPort::OneStop);
  port.setFlowControl(QSerialPort::NoFlowControl);

  protoReader.reset();

  connect(&port, SIGNAL(readyRead()), this, SLOT(ReadHandler()));

  emit openned();
}

void SerialPortDevice::ClossedHandler()
{
  disconnect(&port, SIGNAL(readyRead()), this, SLOT(ReadHandler()));

  emit clossed();
}

qint64 SerialPortDevice::send(const char *data, qint64 size)
{
  char headerBuf[SDR_PROTO_HEADER_BUF_SIZE];
  proto_raw_write_header(size,(UInt8_t*)headerBuf,SDR_PROTO_HEADER_BUF_SIZE);

  QByteArray protoBuf = QByteArray::fromRawData(data, size);
  protoBuf.prepend(headerBuf, SDR_PROTO_HEADER_BUF_SIZE);
  protoBuf.append(SDR_PROTO_FINISH_SYM);

  qint64 len = port.write(protoBuf.data(), protoBuf.size());
  Q_ASSERT(len == protoBuf.size());
//  while (!port.waitForBytesWritten());
  return size;
}

QString SerialPortDevice::errorString()
{
  return port.errorString();
}

void SerialPortDevice::ReadHandler()
{
  QByteArray buf = port.readAll();
#ifdef RX_TRACE
  qDebug() << "rx buf: " << buf.toHex(' ');
#endif
  protoReader.read(buf);
}

void SerialPortDevice::protoBufHandler(QByteArray buf)
{
  emit rx_buf_ready(buf);
}
